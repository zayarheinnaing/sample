//
//  JobsViewModel.swift
//  Sample
//
//  Created by Zayar Naing on 06/12/2020.
//

import Foundation

class JobsViewModel: NSObject {

    private var apiService : ApiService!
    private(set) var jobData : [JobData]! {
        didSet {
            self.bindJobViewModelToController()
        }
    }
    
    var bindJobViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService = ApiService()
        callFuncToFetchJobData()
    }
    
    func callFuncToFetchJobData() {
        self.apiService.fetchJobData { (jobData) in
            self.jobData = jobData
        }
    }
}
