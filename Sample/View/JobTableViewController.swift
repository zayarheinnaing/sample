//
//  JobTableViewController.swift
//  Sample
//
//  Created by Zayar Naing on 08/12/2020.
//

import UIKit
import GoogleSignIn

class JobTableViewController: UITableViewController {
    
    private var jobViewModel: JobsViewModel!
    private var dataSource: GenericTableViewDataSource<JobTableViewCell, JobData>!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        callToViewModelForUIUpdate()
    }
    
    func setup() {
        setupTableView()
        setupNavBarButton()
    }
    
    func setupNavBarButton() {
        let logoutButton = UIBarButtonItem(image: UIImage.init(systemName: "power"), style: .done, target: self, action: #selector(logoutAction))
        self.navigationItem.rightBarButtonItem = logoutButton
    }
    
    func setupTableView() {
        self.tableView.backgroundColor = .lightGray
        
        self.tableView.separatorInset.left = 0
        
        let headerView = UIView()
        let title = UILabel()
        title.text = "Job Available"
        headerView.addSubview(title)
        
        self.tableView.tableHeaderView = headerView
        self.tableView.tableFooterView =  UIView()
        
        headerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            headerView.leadingAnchor.constraint(equalTo: tableView.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: tableView.trailingAnchor)
        ])
        
        title.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10),
            title.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 5),
            title.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -5),
            title.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -10)
        ])
        
        self.tableView.register(JobTableViewCell.self, forCellReuseIdentifier: "JobTableViewCell")
    }
    
    @objc private func logoutAction() {
        GIDSignIn.sharedInstance()?.signOut()
        self.dismiss(animated: true, completion: nil)
    }
    
    func callToViewModelForUIUpdate() {
        self.jobViewModel = JobsViewModel()
        self.jobViewModel.bindJobViewModelToController = {
            self.updateDataSource()
        }
    }
    
    func updateDataSource() {
        self.dataSource = GenericTableViewDataSource(cellIdentifier: "JobTableViewCell", items: self.jobViewModel.jobData!, configureCell: { (cell, jvm) in
            cell.job = jvm
        })
        
        DispatchQueue.main.sync {
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: JobTableViewCell = tableView.cellForRow(at: indexPath) as! JobTableViewCell
        let mapViewController = MapViewController()
        mapViewController.job = cell.job
        self.navigationController?.pushViewController(mapViewController, animated: true)
    }

}
