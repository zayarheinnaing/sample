//
//  MapViewController.swift
//  Sample
//
//  Created by Zayar Naing on 08/12/2020.
//

import UIKit
import MapKit
import GoogleSignIn

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    private var dataSource: GenericTableViewDataSource<UITableViewCell, MKLocalSearchCompletion>!
    var mapView: MKMapView! = {
        let mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return mapView
    }()
    var locationManager: CLLocationManager?
    var job: JobData!
    let autoCompleter: MKLocalSearchCompleter! = {
        let autoCompleter = MKLocalSearchCompleter()
        return autoCompleter
    }()
    let searchBox: UISearchBar = {
        let searchBox = UISearchBar()
        searchBox.placeholder = "Search"
        return searchBox
    }()
    let tableView = UITableView()
    
    private var infoView: InfoView! = {
        let infoView = InfoView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return infoView
    }()
    
    var tableViewHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupNavBar()
        setupSearchBox()
        setupMap()
        setupInfoView()
        setupLocationManager()
        setupTableView()
        
        autoCompleter.delegate = self
    }
    
    func setupTableView() {
        tableView.delegate = self
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableViewHeightConstraint = NSLayoutConstraint(item: tableView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: tableView.contentSize.height)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: searchBox.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -5),
            tableViewHeightConstraint
        ])
        
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.bounces = false
        tableView.separatorInset.left = 0
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "PlaceCell")
        tableView.layoutIfNeeded()
    }
    
    func setupSearchBox() {
        view.addSubview(searchBox)
        searchBox.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBox.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchBox.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            searchBox.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
        
        searchBox.delegate = self
    }
    
    func setupMap() {
        view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: searchBox.bottomAnchor),
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: job.geolocation.lat, longitude: job.geolocation.long)
        mapView.addAnnotation(annotation)
        
        let userTrackingButton = MKUserTrackingButton(mapView: mapView)
        mapView.addSubview(userTrackingButton)
        userTrackingButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            userTrackingButton.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -5),
            userTrackingButton.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 5)
        ])
        
        updateCameraToMarker()
    }
    
    func setupInfoView() {
        infoView.jobIdLabel.text = "Job Number: \(job.job_id)"
        view.addSubview(infoView)
        infoView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            infoView.topAnchor.constraint(equalTo: mapView.bottomAnchor),
            infoView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            infoView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            infoView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            infoView.heightAnchor.constraint(equalToConstant: 80)
        ])
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(updateCameraToMarker))
        infoView.addGestureRecognizer(gesture)
    }
    
    func setupLocationManager() {
        self.locationManager = CLLocationManager()
        if let locationManager = self.locationManager {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    @objc func updateCameraToMarker() {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: job.geolocation.lat, longitude: job.geolocation.long), latitudinalMeters: 5000, longitudinalMeters: 5000)
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
    }
    
    func setupNavBar() {
        let logoutButton = UIBarButtonItem(image: UIImage.init(systemName: "power"), style: .done, target: self, action: #selector(logoutAction))
        navigationItem.rightBarButtonItem = logoutButton
    }
    
    func updateTable(dataSource: UITableViewDataSource?) {
        tableView.dataSource = dataSource
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableViewHeightConstraint.constant = tableView.contentSize.height < 250 ? tableView.contentSize.height : 250
    }
    
    @objc private func logoutAction() {
        GIDSignIn.sharedInstance()?.signOut()
        self.dismiss(animated: true, completion: nil)
    }
}

extension MapViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.updateTable(dataSource: nil)
        } else {
            self.autoCompleter.region = self.mapView.region
            self.autoCompleter.queryFragment = searchText
        }
    }
}

extension MapViewController: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        self.dataSource = GenericTableViewDataSource(cellIdentifier: "PlaceCell", items: completer.results, configureCell: { (cell, result) in
            cell.textLabel?.text = "\(result.title)"
        })
        
        DispatchQueue.main.async {
            self.updateTable(dataSource: self.dataSource)
        }
    }
}

extension MapViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        searchBox.text = cell?.textLabel?.text
        self.updateTable(dataSource: nil)
    }
}
