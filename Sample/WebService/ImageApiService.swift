//
//  ImageApiService.swift
//  Sample
//
//  Created by Zayar Naing on 09/12/2020.
//

import UIKit

class ImageApiService: NSObject {
    
    var sourceURL: URL!
    
    func fetchImage(completion: @escaping(Data) -> ()) {
        URLSession.shared.dataTask(with: sourceURL) {(data,urlresponse,error) in
            if let data = data {
                let imageData: Data = data
                DispatchQueue.main.async {
                    completion(imageData)
                }
            }
        }.resume()
    }

}
