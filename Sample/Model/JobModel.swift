//
//  JobModel.swift
//  Sample
//
//  Created by Zayar Naing on 06/12/2020.
//

import Foundation

struct JobData : Decodable {
    let id, job_id, priority:Int
    let company_name, address: String
    let geolocation: GeolocationData
    
    enum CodingKeys: String, CodingKey {
        case id
        case job_id = "job-id"
        case priority
        case company_name = "company"
        case address, geolocation
        
    }
}

struct GeolocationData  : Decodable {
    let lat, long: Double
    
    enum CodingKeys: String, CodingKey {
        case lat = "latitude"
        case long = "longitude"
    }
}
