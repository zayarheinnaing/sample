//
//  UserViewModel.swift
//  Sample
//
//  Created by Zayar Naing on 09/12/2020.
//

import UIKit
import GoogleSignIn

class UserViewModel: NSObject {
    
    private var apiSerivce: ImageApiService!
    private(set) var image: UIImage! {
        didSet {
            self.bindViewModelToController()
        }
    }
    private(set) var user: User!
    
    var bindViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        
        self.apiSerivce = ImageApiService()
        
        if let currentUser = GIDSignIn.sharedInstance()?.currentUser {
            let userObject = User()
            
            userObject.givenName = currentUser.profile.givenName
            let dimension = round(100 * UIScreen.main.scale)
            userObject.profilePhoto = currentUser.profile.imageURL(withDimension: UInt(dimension))
            
            self.apiSerivce.sourceURL = userObject.profilePhoto
            self.user = userObject
            fetchImage()
        }
    }
    
    func fetchImage() {
        self.apiSerivce.fetchImage {(data) in
            self.image = UIImage(data: data)
        }
    }
}
