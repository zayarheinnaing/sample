//
//  ViewController.swift
//  Sample
//
//  Created by Zayar Naing on 06/12/2020.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController {
    
    let signInButton = GIDSignInButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        if GIDSignIn.sharedInstance()?.currentUser != nil {
            signInOutCompleted()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(signInOutCompleted), name: .signInOutCompleted, object: nil)
        
    }
    
    @objc private func signInOutCompleted() {
        redirect()
    }
    
    func redirect() {
        let jobTableViewController = JobTableViewController()
        let navController = UINavigationController(rootViewController: jobTableViewController)
        navController.modalPresentationStyle = .fullScreen
        DispatchQueue.main.async {
            self.present(navController, animated: true, completion: nil)
        }
    }

}
