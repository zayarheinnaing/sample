//
//  ApiService.swift
//  Sample
//
//  Created by Zayar Naing on 06/12/2020.
//

import Foundation

class ApiService: NSObject {

    private let sourceURL = URL(string: "https://www.haulio.io/joblist.json")!
    
    func fetchJobData(completion : @escaping([JobData]) -> ()) {
        URLSession.shared.dataTask(with: sourceURL){(data,urlresponse,error) in
            if let data = data {
                let jsonDecoder = JSONDecoder()
                let jobData = try! jsonDecoder.decode([JobData].self, from: data)
                completion(jobData)
            }
        }.resume()
    }
}
