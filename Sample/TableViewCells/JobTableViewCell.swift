//
//  JobTableViewCell.swift
//  Sample
//
//  Created by Zayar Naing on 06/12/2020.
//

import UIKit
import MaterialComponents

class JobTableViewCell: UITableViewCell {
    
    let jobNumberLabel: UILabel = {
        let jobNumberLabel = UILabel()
        jobNumberLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return jobNumberLabel
    }()
    let companyLabel: UILabel = {
        let companyLabel = UILabel()
        return companyLabel
    }()
    let addressLabel: UILabel = {
        let addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        return addressLabel
    }()
    
    let acceptButton: MDCButton = {
        let acceptButton = MDCButton()
        acceptButton.setTitle("Accept", for: .normal)
        acceptButton.backgroundColor = .green
        acceptButton.setTitleColor(.black, for: .normal)
        return acceptButton
    }()
    
    var job : JobData! {
        didSet {
            jobNumberLabel.text = "Job Number: \(job.job_id)"
            companyLabel.text = "Company: \(job.company_name)"
            addressLabel.text = "Address: \(job.address)"
        }
    }
    
    func setupCell() {
        contentView.addSubview(acceptButton)
        contentView.addSubview(jobNumberLabel)
        contentView.addSubview(companyLabel)
        contentView.addSubview(addressLabel)

        acceptButton.translatesAutoresizingMaskIntoConstraints = false
        jobNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        companyLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.translatesAutoresizingMaskIntoConstraints = false

        acceptButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true
        acceptButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
        acceptButton.heightAnchor.constraint(equalToConstant: 35).isActive = true

        jobNumberLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true
        jobNumberLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 5).isActive = true

        companyLabel.topAnchor.constraint(equalTo: acceptButton.bottomAnchor, constant: 2).isActive = true
        companyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 5).isActive = true
        companyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true

        addressLabel.topAnchor.constraint(equalTo: companyLabel.bottomAnchor, constant: 2).isActive = true
        addressLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 5).isActive = true
        addressLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
        addressLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)

        // Configure the view for the selected state
    }

}
