//
//  InfoView.swift
//  Sample
//
//  Created by Zayar Naing on 09/12/2020.
//

import UIKit

class InfoView: UIView {
    
    var userModel: UserViewModel!
    
    let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    let givenNameLabel: UILabel =  {
        let givenNameLabel = UILabel()
        givenNameLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return givenNameLabel
    }()
    let jobIdLabel: UILabel = UILabel()
    
    func setup() {
        backgroundColor = .white
        setupImageView()
        setupGivenNameLabel()
        setupJobIdLabel()
        callViewModelToUpdate()
    }

    func setupImageView() {
        addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 50),
            imageView.widthAnchor.constraint(equalToConstant: 50)
        ])
        
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
    }
    
    func setupGivenNameLabel() {
        addSubview(givenNameLabel)
        
        givenNameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            givenNameLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 5),
            givenNameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
            givenNameLabel.bottomAnchor.constraint(equalTo: self.centerYAnchor)
        ])
    }
    
    func setupJobIdLabel() {
        addSubview(jobIdLabel)
        
        jobIdLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            jobIdLabel.topAnchor.constraint(equalTo: self.centerYAnchor),
            jobIdLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 5),
            jobIdLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5)
        ])
    }
    
    func callViewModelToUpdate() {
        self.userModel = UserViewModel()
        self.userModel.bindViewModelToController = {
            self.updateDetails()
        }
    }
    
    func updateDetails() {
        givenNameLabel.text = userModel.user.givenName
        imageView.image = userModel.image
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
